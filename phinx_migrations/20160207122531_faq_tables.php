<?php

use Phinx\Migration\AbstractMigration;

class FaqTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $categoryTable = $this->table('faqcategory');
        $categoryTable->addColumn('name', 'string')
                ->addColumn('shorturl', 'string')
                ->addColumn('description', 'text')
                ->addColumn('categoryorder', 'integer')
                ->addColumn('visible', 'enum', array('values'=>array('Y', 'N'), 'default'=>'Y'))
                ->addColumn('hasimage', 'enum', array('values'=>array('Y', 'N'), 'default'=>'Y'))
                ->addColumn('dateadded', 'datetime')
                ->addColumn('dateupdated', 'datetime')
                ->addIndex(array('name'), array('unique' => true))
                ->addIndex(array('shorturl'), array('unique' => true))
                ->addIndex(array('categoryorder', 'visible', 'hasimage'))
                ->create();
        
        $questionTable = $this->table('faqquestions');
        $questionTable->addColumn('question', 'text')
                ->addColumn('answer', 'text')
                ->addColumn('shorturl', 'string', array('null' => true))
                ->addColumn('faqcategory_id', 'integer')
                ->addColumn('questionorder', 'integer')
                ->addColumn('dateadded', 'datetime')
                ->addColumn('dateupdated', 'datetime')
                ->addForeignKey('faqcategory_id', 'faqcategory', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'))
                ->addIndex(array('questionorder', 'shorturl'))
                ->create();
    }
}
