jQuery.validator.addMethod("regex", function(value, element, param) {
    if (this.optional(element)) {
        return true;
    }

    param = new RegExp(param);

    return param.test(value);
}, "Invalid format.");
