<?php

class AppForms
{
    
    public static $bootstrapLayout = '
        <div class="form-group">
            <div class="col-sm-2">[-LABEL-]</div>
            <div class="col-sm-10">[-ELEMENT-][-EXTRA-]</div>
        </div>
    ';
    
    /**
     * Quick way of getting a value from an array,
     * checks if value exists first
     */
    public static function getValue($array, $key, $default='')
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }
    
    private static function getYesNoOptions()
    {
        return array(
            array('label' => 'Yes', 'value'=>'Y'),
            array('label' => 'No', 'value'=>'N'),
        );
    }
    
    public static function categoryForm($action, $values=array(), $button='Save Category')
    {
        return array (
            'name' => 'categoryForm',
            'method' => 'post',
            'class' => 'form-horizontal formbutton',
            'action' => $action,
            'submitbutton' => $button,
            'elements' => array (
                'name' => array(
                    'type' => 'text',
                    'label' => 'Category name',
                    'value' => self::getValue($values, 'name'),
                    'validation' => array(
                        'required' => TRUE,
                        'remote' => '/remote/name?id='.static::getValue($values, 'id'),
                    ),
                    'cssClass' => 'form-control'
                ),
                'shorturl' => array(
                    'type' => 'text',
                    'label' => 'ShortUrl',
                    'value' => self::getValue($values, 'shorturl'),
                    'validation' => array(
                        'required' => TRUE,
                        'regex' => array (
                            'jsRegex' => '^[a-z_\d]*$',
                            'phpRegex' => '/\A[a-z_\d]*\Z/',
                        ),
                        'remote' => '/remote/shorturl?id='.static::getValue($values, 'id'),
                    ),
                    'validationMessage' => 'Needs to be lower case characters, digits or _ only',
                    'cssClass' => 'form-control'
                ),
                'description' => array(
                    'type' => 'textarea',
                    'label' => 'Description',
                    'value' => self::getValue($values, 'description'),
                    'validation' => array(
                        'required' => TRUE,
                    ),
                    'cssClass' => 'form-control'
                ),
                'image' => array(
                    'type' => 'file',
                    'label' => 'Category Image',
                    'validation' => array(
                        'required' => TRUE,
                        'mimetype' => 'image/png,image/gif,image/jpeg',
                        'extension' => 'gif,png,jpg',
                    ),
                    'validationMessage' => 'Needs to be a valid image',
                    'cssClass' => 'form-control'
                ),
            )
        );
    }
    
    public static function deleteForm($id, $type='category')
    {
        return array (
            'name' => 'deleteForm',
            'method' => 'post',
            'class' => 'form-horizontal formbutton',
            'action' => '/delete'.$type.'/'.$id,
            'submitbutton' => 'Confirm',
            'elements' => array (
                'id' => array(
                    'type' => 'hidden',
                    'value' => $id,
                ),
                'confirm' => array(
                    'type' => 'radiobuttongroup',
                    'label' => 'Are you sure you want to delete this '.$type.'?',
                    'value' => 'N',
                    'divider' => ' &nbsp; ',
                    'options' => static::getYesNoOptions(),
                    //'cssClass' => 'form-control'
                )
            )
        );
    }
    
    public static function categoryImage($id, $button='Save Category Image')
    {
        return array (
            'name' => 'categoryForm',
            'method' => 'post',
            'class' => 'form-horizontal formbutton',
            'action' => '/categoryimage/'.$id,
            'submitbutton' => $button,
            'elements' => array (
                'id' => array(
                    'type' => 'hidden',
                    'value' => $id,
                ),
                'image' => array(
                    'type' => 'file',
                    'label' => 'Category Image',
                    'validation' => array(
                        'required' => TRUE,
                        'mimetype' => 'image/png,image/gif,image/jpeg',
                        'extension' => 'gif,png,jpg',
                    ),
                    'validationMessage' => 'Needs to be a valid image',
                    'cssClass' => 'form-control'
                ),
            )
        );
    }
    
    public static function questionForm($action, $categories, $values=array(), $button='Save Question')
    {
        return array (
            'name' => 'categoryForm',
            'method' => 'post',
            'class' => 'form-horizontal formbutton',
            'action' => $action,
            'submitbutton' => $button,
            'elements' => array (
                'faqcategory_id' => array(
                    'type' => 'dropdown',
                    'label' => 'Category',
                    'value' => self::getValue($values, 'faqcategory_id'),
                    'divider' => ' &nbsp; ',
                    'options' => $categories,
                    'cssClass' => 'form-control',
                    'addempty' => FALSE,
                    'validation' => array(
                        'required' => TRUE,
                    ),
                ),
                'question' => array(
                    'type' => 'textarea',
                    'label' => 'Question',
                    'value' => self::getValue($values, 'question'),
                    'validation' => array(
                        'required' => TRUE,
                    ),
                    'cssClass' => 'form-control'
                ),
                'answer' => array(
                    'type' => 'htmleditor',
                    'label' => 'Answer',
                    'value' => self::getValue($values, 'answer'),
                    'validation' => array(
                        'required' => TRUE,
                    ),
                    'cssClass' => 'form-control'
                ),
            )
        );
    }
    
}