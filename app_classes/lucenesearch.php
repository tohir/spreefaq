<?php

class FaqLuceneSearch
{
    private $index;
    
    
    public function __construct($indexFolder)
    {
        // Try Opening the index, else create it
        try {
            $this->index = ZendSearch\Lucene\Lucene::open($indexFolder);
        } catch (Exception $e) {
            $this->index = ZendSearch\Lucene\Lucene::create($indexFolder);
        }
    }
    
    public function addDocument($id, $categoryId, $question, $answer)
    {
        $doc = new ZendSearch\Lucene\Document();
        
        $doc->addField(ZendSearch\Lucene\Document\Field::Keyword('questionid', $id));
        $doc->addField(ZendSearch\Lucene\Document\Field::Keyword('categoryid', $categoryId));
        $doc->addField(ZendSearch\Lucene\Document\Field::Text('question', $question));
        $doc->addField(ZendSearch\Lucene\Document\Field::UnStored('answer', $answer));
        
        $this->index->addDocument($doc);
    }
    
    public function updateDocument($id, $categoryId, $question, $answer)
    {
        $this->deleteDocument($id);
        $this->addDocument($id, $categoryId, $question, $answer);
    }
    
    public function deleteDocument($id)
    {
        foreach ($this->index->find('questionid:'.$id) as $hit)
        {
            $this->index->delete($hit->id);
        }
    }
    
    public function search($query)
    {
        $results = array();
        
        foreach($this->index->find($query) as $rec)
        {
            $results[] = array(
                'id' => $rec->questionid,
                'category' => $rec->categoryid,
                'question' => $rec->question,
                'score' => $rec->score,
            );
        }
        
        return $results;
    }
}