<?php

class CurlCaller
{
    private static $httpcode;

    public static function call($method, $url, $data=FALSE, $httpHeaders=array(), $username='', $password='')
    {
        $curl = curl_init();
        
        switch (strtoupper($method))
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
                
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                } else {
                    // Prevents 413 - Request Entity Too Large
                    curl_setopt($curl, CURLOPT_POSTFIELDS, array());
                }
                break;
            
            case "PUT":
            case "DELETE":
            case "PATCH":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            
            default: // GET
                if ($data){
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }
        
        $options = array(
            CURLOPT_URL             => $url,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HEADER          => false, // true returns the Header info as a string
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_SSL_VERIFYHOST  => '0',
            CURLOPT_SSL_VERIFYPEER  => '0', // Ignore certificate warning
            CURLOPT_USERAGENT       => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
            CURLOPT_VERBOSE         => true,
        );
        
        // Set HTTP Headers if set
        if (is_array($httpHeaders) && !empty($httpHeaders)) {
            $options[CURLOPT_HTTPHEADER] = $httpHeaders;
        }
        
        curl_setopt_array($curl, $options);
        
        
        if (!empty($username) && !empty($password)) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "{$username}:{$password}");
        }
        
        $output = curl_exec($curl);
        
        static::$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);
        
        return $output;
    }
    
    public static function getLastStatusCode()
    {
        return static::$httpcode;
    }
}