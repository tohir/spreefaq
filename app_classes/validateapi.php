<?php

use Respect\Validation\Validator as RespectValidator;

class ValidAPIRequests
{

    public static function validateCategory($data)
    {
        $validator = RespectValidator::key('name', RespectValidator::stringType()->notEmpty())
            ->key('shorturl', RespectValidator::stringType()->notEmpty())
            ->key('description', RespectValidator::stringType()->notEmpty())
            ->key('visible', RespectValidator::in(['Y', 'N']), FALSE) // FALSE for optional
        ;
            
        return $validator->validate($data);
    }
    
    public static function validateQuestion($data)
    {
        $validator = RespectValidator::key('question', RespectValidator::stringType()->notEmpty())
            ->key('answer', RespectValidator::stringType()->notEmpty())
            ->key('faqcategory_id', RespectValidator::intVal()->notEmpty())
            ->key('shorturl', RespectValidator::stringType(), FALSE) // FALSE for optional
        ;
            
        return $validator->validate($data);
    }
    
    public static function validateFile($file, $fieldName, $type='')
    {
        $fileValidate = RespectValidator::key($fieldName, RespectValidator::arrayType()->notEmpty());
        
        if (!$fileValidate->validate($file)) {
            return 'field';
        }
        
        if (!empty($type)) {
            $types = explode('|', $type);
            
            $mimeFound  = FALSE;
            
            foreach ($types as $mime) {
                if (strpos($file[$fieldName]['type'], $mime) === FALSE) {
                    // Not Found
                } else {
                    $mimeFound = TRUE;
                }
            }
            
            if ($mimeFound === FALSE) {
                return 'type';
            }
        }
        
        return TRUE;
    }
    
}