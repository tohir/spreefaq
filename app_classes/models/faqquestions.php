<?php

class DBModel_FaqQuestions extends \Tohir\DBModel
{

    /**
     * @var string Name of Table
     */
    protected $tableName = 'faqquestions';

    /**
     * @var string Primary of Table
     */
    protected $primaryKey = 'id';

    /**
     * @var string The column holding the Date Inserted Field - auto populated if set
     */
    protected $dateInsertColumn = 'dateadded';
    
    /**
     * @var string The column holding the Date Updated Field - auto populated if set
     */
    protected $dateUpdateColumn = 'dateupdated';

    protected $tableColumns = array(
            'question',
            'answer',
            //'shorturl',
            'faqcategory_id',
            'questionorder',
        );  
    
    private function purifyColumn(&$data)
    {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $data = $purifier->purify($data);
    }
    
    private function getCategoryLastOrder($categoryId)
    {
        $lastOrder = $this->db->selectFirst($this->tableName, array('faqcategory_id'=>$categoryId), array('questionorder'=>'DESC'));
        
        return empty($lastOrder) ? 1 : $lastOrder['questionorder']+1;
    }
    
    protected function hook_before_add($data)
    {
        // Get Last Order for Category
        $data['questionorder'] = $this->getCategoryLastOrder($data['faqcategory_id']);
        
        // Purify Answer as it comes via WYSIWYG
        $this->purifyColumn($data['answer']);
        
        return $data;
    }
    
    protected function hook_before_update($data)
    {
        if (isset($data['answer'])) {
            // Purify Answer as it comes via WYSIWYG
            $this->purifyColumn($data['answer']);
        }
        
        return $data;
    }
    
    public function getCategoryQuestions($categoryId)
    {
        return $this->db->select($this->tableName, array('faqcategory_id'=>$categoryId), NULL, NULL, array('questionorder'=>'ASC'));
    }
    
    public function updateInclCategory($id, $data)
    {
        $data['questionorder'] = $this->getCategoryLastOrder($data['faqcategory_id']);
        
        return $this->update($id, $data);
    }
    
    public function getPreviousQuestion($categoryId, $questionOrder)
    {
        $query = 'SELECT * FROM faqquestions WHERE faqcategory_id=:faqCategory AND questionorder < :questionOrder ORDER BY questionorder DESC';
        
        return $this->db->queryFirst($query, array('faqCategory'=>$categoryId, 'questionOrder'=>$questionOrder));
    }
    
    public function getNextQuestion($categoryId, $questionOrder)
    {
        $query = 'SELECT * FROM faqquestions WHERE faqcategory_id=:faqCategory AND questionorder > :questionOrder ORDER BY questionorder';
        
        return $this->db->queryFirst($query, array('faqCategory'=>$categoryId, 'questionOrder'=>$questionOrder));
    }
    
    public function getQuestionsOrdered($category)
    {
        return $this->db->select($this->tableName, array('faqcategory_id'=>$category), NULL, NULL, array('questionorder'=>'ASC'));
    }
    
    public function deleteRecord($id)
    {
        return $this->db->delete($this->tableName, array('id'=>$id));
    }

    
}
