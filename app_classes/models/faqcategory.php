<?php

class DBModel_FaqCategory extends \Tohir\DBModel
{

    /**
     * @var string Name of Table
     */
    protected $tableName = 'faqcategory';

    /**
     * @var string Primary of Table
     */
    protected $primaryKey = 'id';

    /**
     * @var string The column holding the Date Inserted Field - auto populated if set
     */
    protected $dateInsertColumn = 'dateadded';
    
    /**
     * @var string The column holding the Date Updated Field - auto populated if set
     */
    protected $dateUpdateColumn = 'dateupdated';

    protected $tableColumns = array(
            'name',
            'shorturl',
            'description',
            'categoryorder',
            'visible',
            'hasimage',
        );
    
    protected function hook_before_add($data)
    {
        $lastOrder = $this->db->selectFirst($this->tableName, NULL, array('categoryorder'=>'DESC'));
        
        if (empty($lastOrder)) {
            $data['categoryorder'] = 1;
        } else {
            $data['categoryorder'] = $lastOrder['categoryorder']+1;
        }
        
        return $data;
    }
    
    protected function hook_before_update($data)
    {
        return $data;
    }
    
    public function getCategoriesOrdered()
    {
        return $this->db->select($this->tableName, array('visible'=>'Y'), NULL, NULL, array('categoryorder'=>'ASC'));
    }
    
    public function deleteRecord($id)
    {
        return $this->db->delete($this->tableName, array('id'=>$id));
    }
    
    public function getPreviousCategory($categoryOrder)
    {
        $query = 'SELECT * FROM faqcategory WHERE categoryorder < :categoryOrder ORDER BY categoryorder DESC';
        
        return $this->db->queryFirst($query, array('categoryOrder'=>$categoryOrder));
    }
    
    public function getNextCategory($categoryOrder)
    {
        $query = 'SELECT * FROM faqcategory WHERE categoryorder > :categoryOrder ORDER BY categoryorder';
        
        return $this->db->queryFirst($query, array('categoryOrder'=>$categoryOrder));
    }
    
}
