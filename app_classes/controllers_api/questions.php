<?php

class QuestionsController extends SlimRunController
{
    protected function init()
    {
        $this->category = $this->db->loadModel('FaqCategory');
        $this->question = $this->db->loadModel('FaqQuestions');
        
        $this->searchIndex = new FaqLuceneSearch(APPLICATION_PATH.'/cache/luceneindex');
    }
    
    protected function questions_get($id)
    {
        // Check Category
        $category = $this->category->get($id);
        
        if (empty($category)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Category'));
        }
        
        return json_encode(array('status'=>'ok', 'data'=>$this->question->getQuestionsOrdered($id)));
    }
    
    protected function questions_post($id)
    {
        $data = array(
            'question'          => $this->postValue('question'),
            'answer'            => $this->postValue('answer'),
            'shorturl'          => $this->postValue('shorturl'),
            'faqcategory_id'    => $id,
        );
        
        // Validate Header
        if (!ValidAPIRequests::validateQuestion($data)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Params'));
        }
        
        // Check Category
        $category = $this->category->get($id);
        
        if (empty($category)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Category'));
        }
        
        $result = $this->question->add($data);
        
        if ($result === FALSE) {
            $this->setStatusCode(500);
            return json_encode(array('status'=>'notok', 'error'=>'Error Saving Question', 'dberror'=>$this->category->getErrorMessage()));
        } else {
            
            $this->searchIndex->addDocument($result, $data['faqcategory_id'], $data['question'], $data['answer']);
            
            $this->setStatusCode(201);
            return json_encode(array('status'=>'ok', 'id'=>$result, 'record'=>$this->question->get($result)));
        }
        
    }
    
    protected function action_get($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        } else {
            return json_encode(array('status'=>'ok', 'id'=>$id, 'record'=>$record));
        }
    }
    
    protected function action_put($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $data = array(
            'question'          => $this->inputValue('question'),
            'answer'            => $this->inputValue('answer'),
            'shorturl'          => $this->inputValue('shorturl'),
            'faqcategory_id'    => $this->inputValue('faqcategory_id'),
        );
        
        if (!ValidAPIRequests::validateQuestion($data)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Params'));
        }
        
        // Check if Category Has Changed
        if ($record['faqcategory_id'] != $this->inputValue('faqcategory_id')) {
            
            // Check Category
            $category = $this->category->get($this->inputValue('faqcategory_id'));
            
            if (empty($category)) {
                $this->setStatusCode(400);
                return json_encode(array('status'=>'notok', 'error'=>'Invalid Category'));
            }
            
            $result = $this->question->updateInclCategory($id, $data);
            
        } else {
            $result = $this->question->update($id, $data);
        }
        
        // Send Results
        if ($result === FALSE) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Error Saving Question', 'dberror'=>$this->question->getErrorMessage()));
        } else {
            $this->searchIndex->updateDocument($id, $data['faqcategory_id'], $data['question'], $data['answer']);
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'id'=>$result, 'record'=>$this->question->get($id)));
        }
        
    }
    
    protected function action_delete($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $result = $this->question->deleteRecord($id);
        
        if ($result === FALSE) {
            $this->setStatusCode(500);
        } else {
            $this->searchIndex->deleteDocument($id);
            $this->setStatusCode(204);
        }

    }
    
    protected function moveup_patch($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $prevRecord = $this->question->getPreviousQuestion($record['faqcategory_id'], $record['questionorder']);
        
        if ($prevRecord === FALSE) {
            $this->setStatusCode(200);
            return json_encode(array('status'=>'ok', 'result'=>'Not moved'));
        } else {
            
            $this->question->update($record['id'], array('questionorder' => $prevRecord['questionorder']));
            $this->question->update($prevRecord['id'], array('questionorder' => $record['questionorder']));
            
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'result'=>'Moved'));
        }
    }
    
    protected function movedown_patch($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $nextRecord = $this->question->getNextQuestion($record['faqcategory_id'], $record['questionorder']);
        
        if ($nextRecord === FALSE) {
            $this->setStatusCode(200);
            return json_encode(array('status'=>'ok', 'result'=>'Not moved'));
        } else {
            
            $this->question->update($record['id'], array('questionorder' => $nextRecord['questionorder']));
            $this->question->update($nextRecord['id'], array('questionorder' => $record['questionorder']));
            
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'result'=>'Moved'));
        }
    }
    
    protected function prevquestion_get($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $prevRecord = $this->question->getPreviousQuestion($record['faqcategory_id'], $record['questionorder']);
        
        if ($prevRecord === FALSE) {
            return json_encode(array('status'=>'notok', 'result'=>'No Previous Question'));
        } else {
            return json_encode(array('status'=>'ok', 'record'=>$prevRecord));
        }
    }
    
    protected function nextquestion_get($id)
    {
        $record = $this->question->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $nextRecord = $this->question->getNextQuestion($record['faqcategory_id'], $record['questionorder']);
        
        if ($nextRecord === FALSE) {
            return json_encode(array('status'=>'notok', 'result'=>'No Next Question'));
        } else {
            return json_encode(array('status'=>'ok', 'record'=>$nextRecord));
        }
    }
    
    protected function search_get()
    {
        $term = $this->getValue('term');
        
        return json_encode(array('status'=>'ok', 'data'=>$this->searchIndex->search($term)));
    }
    
}