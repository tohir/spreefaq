<?php


class CategoriesController extends SlimRunController
{
    protected function init()
    {
        $this->category = $this->db->loadModel('FaqCategory');
    }
    
    protected function list_get()
    {
        return json_encode(array('status'=>'ok', 'data'=>$this->category->getCategoriesOrdered()));
    }
    
    protected function namecheck_get()
    {
        $name = $this->getValue('name');
        $id = $this->getValue('id');
        
        $record = $this->category->getRow('name', $name);
        
        if ($record == FALSE) {
            return json_encode(array('status'=>'ok', 'name'=>'available'));
        } elseif ($record['id'] == $id) {
            return json_encode(array('status'=>'ok', 'name'=>'current'));
        } else {
            return json_encode(array('status'=>'notok', 'name'=>'taken'));
        }
    }
    
    protected function shorturl_get()
    {
        $shorturl = $this->getValue('shorturl');
        $id = $this->getValue('id');
        
        $record = $this->category->getRow('shorturl', $shorturl);
        
        if ($record == FALSE) {
            return json_encode(array('status'=>'ok', 'shorturl'=>'available'));
        } elseif ($record['id'] == $id) {
            return json_encode(array('status'=>'ok', 'shorturl'=>'current'));
        } else {
            return json_encode(array('status'=>'notok', 'shorturl'=>'taken'));
        }
    }
    
    protected function new_post()
    {
        $data = array(
            'name'          => $this->postValue('name'),
            'shorturl'      => $this->postValue('shorturl'),
            'description'   => $this->postValue('description'),
            'visible'       => $this->postValue('visible', 'Y'),
        );
        
        $fileValidation = ValidAPIRequests::validateFile($_FILES, 'image', 'image/');
        
        if ($fileValidation !== TRUE) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Category Image'));
        }
        
        if (!ValidAPIRequests::validateCategory($data)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Params'));
        }
        
        $data['hasimage'] = ($_FILES['image']['error'] == 0) ? 'Y' : 'N';
        
        $result = $this->category->add($data);
        
        if ($result === FALSE) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Duplicate Name or Short URL', 'dberror'=>$this->category->getErrorMessage()));
        } else {
            
            if ($data['hasimage'] == 'Y') {
                $this->saveImage($result);
            }
            
            $this->setStatusCode(201);
            return json_encode(array('status'=>'ok', 'id'=>$result, 'record'=>$this->category->get($result)));
        }
    }
    
    /**
     * Method to save Uploaded Image to files folder
     * @param int $id Record Id of Category
     */
    private function saveImage($id)
    {
        $destination = APPLICATION_PATH.'/public_api/files/'.$id.'.png';
        
        try {
            \WideImage\WideImage::load($_FILES['image']['tmp_name'])->resize(200, 200)->saveToFile($destination);
            return TRUE;
        } catch (Exception $e) {
            $this->category->update($id, array('hasimage' => 'N'));
            return FALSE;
        }
    }
    
    protected function image_post($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $fileValidation = ValidAPIRequests::validateFile($_FILES, 'image', 'image/');
        
        if ($fileValidation !== TRUE) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Category Image'));
        }
        
        if ($this->saveImage($id)) {
            $this->category->update($id, array('hasimage' => 'Y'));
            
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'result'=>'Image Updated'));
        } else {
            $this->setStatusCode(500);
            return json_encode(array('status'=>'ok', 'result'=>'Problem saving image'));
        }
    }
    
    protected function image_delete($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        @unlink(APPLICATION_PATH.'/public_api/files/'.$id.'.png');
        
        $this->category->update($id, array('hasimage' => 'N'));
        $this->setStatusCode(204);
        
        return json_encode(array('status'=>'ok', 'result'=>'Image Removed'));
    }
    
    protected function action_get($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        } else {
            return json_encode(array('status'=>'ok', 'id'=>$id, 'record'=>$record));
        }
    }
    
    protected function byShortUrl_get($url)
    {
        $record = $this->category->getRow('shorturl', $url);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        } else {
            return json_encode(array('status'=>'ok', 'id'=>$record['id'], 'record'=>$record));
        }
    }
    
    protected function action_put($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $data = array(
            'name'          => $this->inputValue('name'),
            'shorturl'      => $this->inputValue('shorturl'),
            'description'   => $this->inputValue('description'),
            'visible'       => $this->inputValue('visible', 'Y'),
        );
        
        if (!ValidAPIRequests::validateCategory($data)) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Invalid Params'));
        }
        
        $result = $this->category->update($id, $data);
        
        // @todo - If visibile change - update lucene
        
        if ($result === FALSE) {
            $this->setStatusCode(400);
            return json_encode(array('status'=>'notok', 'error'=>'Duplicate Name or Short URL', 'dberror'=>$this->category->getErrorMessage()));
        } else {
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'id'=>$result, 'record'=>$this->category->get($id)));
        }
        
    }
    
    protected function action_delete($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $result = $this->category->deleteRecord($id);
        
        if ($result === FALSE) {
            $this->setStatusCode(500);
        } else {
            
            $questionsModel = $this->db->loadModel('FaqQuestions');
            $questions = $questionsModel->getCategoryQuestions($id);
            
            if ($empty($questions)) {
                $searchIndex = new FaqLuceneSearch(APPLICATION_PATH.'/cache/luceneindex');
                
                foreach ($questions as $question)
                {
                    $searchIndex->deleteDocument($question['id']);
                }
            }
            
            
            $this->setStatusCode(204);
        }

    }
    
    protected function moveup_patch($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $prevRecord = $this->category->getPreviousCategory($record['categoryorder']);
        
        if ($prevRecord === FALSE) {
            $this->setStatusCode(200);
            return json_encode(array('status'=>'ok', 'result'=>'Not moved'));
        } else {
            
            $this->category->update($record['id'], array('categoryorder' => $prevRecord['categoryorder']));
            $this->category->update($prevRecord['id'], array('categoryorder' => $record['categoryorder']));
            
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'result'=>'Moved'));
        }
    }
    
    protected function movedown_patch($id)
    {
        $record = $this->category->get($id);
        
        if ($record === FALSE) {
            $this->setStatusCode(404);
            return json_encode(array('status'=>'notok', 'error'=>'Record Not Found'));
        }
        
        $nextRecord = $this->category->getNextCategory($record['categoryorder']);
        
        if ($nextRecord === FALSE) {
            $this->setStatusCode(200);
            return json_encode(array('status'=>'ok', 'result'=>'Not moved'));
        } else {
            
            $this->category->update($record['id'], array('categoryorder' => $nextRecord['categoryorder']));
            $this->category->update($nextRecord['id'], array('categoryorder' => $record['categoryorder']));
            
            $this->setStatusCode(202);
            return json_encode(array('status'=>'ok', 'result'=>'Moved'));
        }
    }
    
}