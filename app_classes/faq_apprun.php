<?php

use \SlimRunner\AppConfig as AppConfig;

class FaqAppRun extends \SlimRunner\SlimRunner
{
    protected function init()
    {
        $this->template->setTemplateDir(APPLICATION_PATH.'/templates/faq/');
        
        $this->setPageTemplate(AppConfig::get('faqtemplates', 'page'));
        $this->setLayoutTemplate(AppConfig::get('faqtemplates', 'layout'));
        
        $this->template->persistTemplateVar('apiUrl', AppConfig::get('urls', 'spreeapi'));
        $this->template->persistTemplateVar('appTitle', 'Frequent Asked Questions');
        
        $this->registerRoutes(array(
            array('/',                      'tab:home',             'home',                 'get|post'),
            array('/addcategory',           'tab:addcategory',      'addcategory',          'get|post'),
            array('/editcategory/:id',      'tab:categories',       'editcategory',         'get|post',     array('id' => '\d+')),
            array('/deletecategory/:id',    'tab:categories',       'deletecategory',       'get|post',     array('id' => '\d+')),
            array('/categoryimage/:id',     'tab:categories',       'categoryimage',        'get|post',     array('id' => '\d+')),
            array('/categories',            'tab:categories',       'categories',           'get'),
            array('/categorymoveup/:id',    'tab:categories',       'category_moveup',      'get',          array('id' => '\d+')),
            array('/categorymovedown/:id',  'tab:categories',       'category_movedown',    'get',          array('id' => '\d+')),
            array('/remote/name',           'ajax',                 'remote_name',          'get'),
            array('/remote/shorturl',       'ajax',                 'remote_shorturl',      'get'),
            
            array('/view/:shorturl',        'tab:home',             'viewcategory',         'get'),
            
            array('/addquestion(/:id)',     'tab:addquestion',      'addquestion',          'get|post',     array('id' => '\d+')),
            array('/editquestion/:id',      'tab:categories',       'editquestion',         'get|post',     array('id' => '\d+')),
            array('/deletequestion/:id',    'tab:categories',       'deletequestion',       'get|post',     array('id' => '\d+')),
            array('/questionmoveup/:id',    'tab:categories',       'question_moveup',      'get',          array('id' => '\d+')),
            array('/questionmovedown/:id',  'tab:categories',       'question_movedown',    'get',          array('id' => '\d+')),
            array('/viewquestion/:id',      'tab:categories',       'viewquestion',         'get',          array('id' => '\d+')),
            
            array('/search',                'tab:home',             'search',               'get'),
            array('/searchajax',            'tab:home',             'searchajax',           'get'),
        ));
    }
    
    protected function accesscheck_ajax()
    {
        $this->setIsAjaxResponse();
    }
    
    protected function accesscheck_tab($tab)
    {
        $this->template->persistTemplateVar('currentTab', $tab);
    }
    
    private function runCurlCall($route, $method, $params=array())
    {
        $url = AppConfig::get('urls', 'spreeapi').$route;
        
        $response = CurlCaller::call($method, $url, $params);
        
        return json_decode($response, TRUE);
    }
    
    
    protected function remote_name_get()
    {
        $result = $this->runCurlCall('/category/namecheck', 'GET', array('name'=>$this->getValue('name'), 'id'=>$this->getValue('id')));
        
        if ($result['status'] == 'ok') {
            return json_encode(TRUE);
        } else {
            return json_encode('This name has already been used');
        }
    }
    
    protected function remote_shorturl_get()
    {
        $result = $this->runCurlCall('/category/shorturlcheck', 'GET', array('shorturl'=>$this->getValue('shorturl'), 'id'=>$this->getValue('id')));
        
        if ($result['status'] == 'ok') {
            return json_encode(TRUE);
        } else {
            return json_encode('This shorturl has already been used');
        }
    }
    
    protected function home_get()
    {
        $categories = $this->runCurlCall('/category', 'GET');
        
        if (empty($categories) || empty($categories['data'])) {
            return 'First add a category';
        }
        
        return $this->template->loadTemplate('content/home.tpl', array('categories'=>$categories['data']));
    }
    
    protected function categories_get()
    {
        $categories = $this->runCurlCall('/category', 'GET');
        
        if ($categories['status'] == 'ok') {
            return $this->template->loadTemplate('content/categories.tpl', array(
                'categories'=>$categories['data'],
                'random'=>rand(1000,40000), // To overcome cached images
                'lastid'=>$this->getValue('id')
            ));
        } else {
            return 'Error Loading Categories';
        }
    }
    
    protected function addcategory_get()
    {
        $form = new FormGenerator(AppForms::categoryForm('/addcategory'), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Add New Category',
                'form' => $form->display(),
            ));
    }
    
    protected function addcategory_post()
    {
        $form = new FormGenerator(AppForms::categoryForm('/addcategory'), AppForms::$bootstrapLayout, TRUE);
        
        $formIsValid = $form->isValid();
        
        if (!$formIsValid) {
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Add New Category',
                    'form' => $form->display(),
                ));
        }
        
        $formData = $form->getFormData();
        
        $imageInfo = $formData['image'];
        
        $formData['image'] = new CURLFile($imageInfo['filepath'], $imageInfo['mimetype']);
        
        $result = $this->runCurlCall('/category', 'POST', $formData);
        
        if ($result === FALSE) {
            
            $form->addFormValidationError('There was an error saving the category');
            
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Add New Category',
                    'form' => $form->display(),
                ));
                                     
        } else {
            return $this->redirect('/categories?msg=added');
        }
    }
    
    protected function editcategory_get($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $formStructure = AppForms::categoryForm('/editcategory/'.$id, $category['record'], 'Update Category');
        
        unset($formStructure['elements']['image']);
        
        $form = new FormGenerator($formStructure, AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Edit Category: '.$category['record']['name'],
                'form' => $form->display(),
            ));
    }
    
    protected function editcategory_post($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $formStructure = AppForms::categoryForm('/editcategory/'.$id, $category['record'], 'Update Category');
        
        unset($formStructure['elements']['image']);
        
        $form = new FormGenerator($formStructure, AppForms::$bootstrapLayout, TRUE);
        
        $formIsValid = $form->isValid();
        
        if (!$formIsValid) {
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Edit Category: '.$category['record']['name'],
                    'form' => $form->display(),
                ));
        }
        
        $formData = $form->getFormData();
        
        $result = $this->runCurlCall('/category/'.$id, 'PUT', $formData);
        
        if ($result['status'] == 'ok') {
            return $this->redirect('/categories?msg=update&id='.$id);
        } else {
            $form->addFormValidationError('There was an error saving the category');
            
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Edit Category: '.$category['record']['name'],
                    'form' => $form->display(),
                ));
        }
    }
    
    protected function deletecategory_get($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $form = new FormGenerator(AppForms::deleteForm($id, 'category'), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Delete Category: '.$category['record']['name'],
                'form' => $form->display(),
            ));
    }
    
    protected function deletecategory_post($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $form = new FormGenerator(AppForms::deleteForm($id, 'category'), AppForms::$bootstrapLayout, TRUE);
        
        $formData = $form->getFormData();
        
        if (AppForms::getValue($formData, 'id') == $id && AppForms::getValue($formData, 'confirm', 'N') == 'Y') {
            
            $result = $this->runCurlCall('/category/'.$id, 'DELETE');
            
            //echo CurlCaller::getLastStatusCode();
            
            return $this->redirect('/categories?msg=deleted&id='.$id);
        } else {
            return $this->redirect('/categories?msg=deletecancelled&id='.$id);
        }
    }
    
    protected function categoryimage_get($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $form = new FormGenerator(AppForms::categoryImage($id), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Change Category Image: '.$category['record']['name'],
                'form' => $form->display(),
            ));
    }
    
    protected function categoryimage_post($id)
    {
        $category = $this->runCurlCall('/category/'.$id, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $form = new FormGenerator(AppForms::categoryImage($id), AppForms::$bootstrapLayout, TRUE);
        
        $formIsValid = $form->isValid();
        
        if (!$formIsValid) {
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Change Category Image: '.$category['record']['name'],
                    'form' => $form->display(),
                ));
        }
        
        $formData = $form->getFormData();
        
        $imageInfo = $formData['image'];
        
        $formData['image'] = new CURLFile($imageInfo['filepath'], $imageInfo['mimetype']);
        
        $result = $this->runCurlCall('/category/'.$id.'/image', 'POST', $formData);
        
        if ($result === FALSE) {
            
            $form->addFormValidationError('There was an error saving the category image');
            
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Change Category Image: '.$category['record']['name'],
                    'form' => $form->display(),
                ));
                                     
        } else {
            return $this->redirect('/categories?msg=imgupdated&id='.$id);
        }
        
    }
    
    protected function category_moveup_get($id)
    {
        $this->runCurlCall('/category/'.$id.'/moveup', 'PATCH');
        return $this->redirect('/categories?msg=update&id='.$id);
    }
    
    protected function category_movedown_get($id)
    {
        $this->runCurlCall('/category/'.$id.'/movedown', 'PATCH');
        return $this->redirect('/categories?msg=update&id='.$id);
    }
    
    protected function viewcategory_get($shortUrl)
    {
        $category = $this->runCurlCall('/category/byshorturl/'.$shortUrl, 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $questions = $this->runCurlCall('/category/'.$category['record']['id'].'/questions', 'GET');
        
        return $this->template->loadTemplate('content/categorypage.tpl', array(
                    'category' => $category['record'],
                    'questions' => $questions['data'],
                ));
        
    }
    
    protected function addquestion_get($id=NULL)
    {
        $categories = $this->runCurlCall('/category', 'GET');
        
        if (empty($categories) || empty($categories['data'])) {
            return 'First add a category';
        }
        
        $form = new FormGenerator(AppForms::questionForm('/addquestion', $this->generateCategoryOptions($categories['data']), array('faqcategory_id'=>$id)), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Add New Question',
                'form' => $form->display(),
            ));
    }
    
    protected function addquestion_post($id=NULL)
    {
        $categories = $this->runCurlCall('/category', 'GET');
        
        if (empty($categories) || empty($categories['data'])) {
            return 'First add a category';
        }
        
        $form = new FormGenerator(AppForms::questionForm('/addquestion', $this->generateCategoryOptions($categories['data']), array('faqcategory_id'=>$id)), AppForms::$bootstrapLayout, TRUE);
        
        if (!$form->isValid()) {
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Add New Question',
                    'form' => $form->display(),
                ));
        } else {
            
            $formData = $form->getFormData();
            
            $category = $this->runCurlCall('/category/'.$formData['faqcategory_id'], 'GET');
            
            if (empty($category) || $category['status'] != 'ok') {
                return 'Error: Category not found';
            }

            $result = $this->runCurlCall('/category/'.$formData['faqcategory_id'].'/questions', 'POST', $formData);
            
            if ($result === FALSE) {
                
                $form->addFormValidationError('There was an error saving the question');
                
                return $this->template->loadTemplate('content/form.tpl', array(
                        'title' => 'Add New question',
                        'form' => $form->display(),
                    ));
                                         
            } else {
                return $this->redirect('/view/'.$category['record']['shorturl'].'?msg=questionaddedadded');
            }
        }
    }
    
    private function generateCategoryOptions($categories)
    {
        $response = array();
        
        foreach ($categories as $category)
        {
            $response[] = array('label'=>$category['name'], 'value'=>$category['id']);
        }
        
        return $response;
    }
    
    protected function deletequestion_get($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return 'Error: Question not found';
        }
        
        $form = new FormGenerator(AppForms::deleteForm($id, 'question'), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Delete Question: '.$question['record']['question'],
                'form' => $form->display(),
            ));
    }
    
    protected function deletequestion_post($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return 'Error: Question not found';
        }
        
        $form = new FormGenerator(AppForms::deleteForm($id, 'question'), AppForms::$bootstrapLayout, TRUE);
        
        $formData = $form->getFormData();
        
        $category = $this->runCurlCall('/category/'.$question['record']['faqcategory_id'], 'GET');
            
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        if (AppForms::getValue($formData, 'id') == $id && AppForms::getValue($formData, 'confirm', 'N') == 'Y') {
            
            $result = $this->runCurlCall('/question/'.$id, 'DELETE');
            
            //echo CurlCaller::getLastStatusCode();
            
            return $this->redirect('/view/'.$category['record']['shorturl'].'?msg=questiondeleted&id='.$id);
        } else {
            return $this->redirect('/view/'.$category['record']['shorturl'].'?msg=questiondeletecancelled&id='.$id);
        }
    }
    
    protected function question_moveup_get($id)
    {
        $this->runCurlCall('/question/'.$id.'/moveup', 'PATCH');
        return $this->redirect('/view/'.$this->getQuestionCategoryShortUrl($id).'?msg=questionmoved&id='.$id);
    }
    
    protected function question_movedown_get($id)
    {
        $this->runCurlCall('/question/'.$id.'/movedown', 'PATCH');
        return $this->redirect('/view/'.$this->getQuestionCategoryShortUrl($id).'?msg=questionmoved&id='.$id);
    }
    
    private function getQuestionCategoryShortUrl($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return FALSE;
        }
        
        $category = $this->runCurlCall('/category/'.$question['record']['faqcategory_id'], 'GET');
            
        if (empty($category) || $category['status'] != 'ok') {
            return FALSE;
        } else {
            return $category['record']['shorturl'];
        }
    }
    
    protected function editquestion_get($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return 'Error: Question not found';
        }
        
        $categories = $this->runCurlCall('/category', 'GET');
        
        if (empty($categories) || empty($categories['data'])) {
            return 'First add a category';
        }
        
        $form = new FormGenerator(AppForms::questionForm('/editquestion/'.$id, $this->generateCategoryOptions($categories['data']), $question['record']), AppForms::$bootstrapLayout);
        
        return $this->template->loadTemplate('content/form.tpl', array(
                'title' => 'Edit Question: '.$question['record']['question'],
                'form' => $form->display(),
            ));
    }
    
    protected function editquestion_post($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return 'Error: Question not found';
        }
        
        $categories = $this->runCurlCall('/category', 'GET');
        
        if (empty($categories) || empty($categories['data'])) {
            return 'First add a category';
        }
        
        $form = new FormGenerator(AppForms::questionForm('/editquestion/'.$id, $this->generateCategoryOptions($categories['data']), $question['record']), AppForms::$bootstrapLayout, TRUE);
        
        if (!$form->isValid()) {
            return $this->template->loadTemplate('content/form.tpl', array(
                    'title' => 'Edit Question: '.$question['record']['question'],
                    'form' => $form->display(),
                ));
        } else {
            
            $formData = $form->getFormData();
            
            $category = $this->runCurlCall('/category/'.$formData['faqcategory_id'], 'GET');
            
            if (empty($category) || $category['status'] != 'ok') {
                return 'Error: Category not found';
            }

            $result = $this->runCurlCall('/question/'.$id, 'PUT', $formData);
            
            if ($result === FALSE) {
                
                $form->addFormValidationError('There was an error saving the question');
                
                return $this->template->loadTemplate('content/form.tpl', array(
                        'title' => 'Edit Question: '.$question['record']['question'],
                        'form' => $form->display(),
                    ));
                                         
            } else {
                return $this->redirect('/view/'.$category['record']['shorturl'].'?msg=questionupdated');
            }
        }
    }
    
    protected function viewquestion_get($id)
    {
        $question = $this->runCurlCall('/question/'.$id, 'GET');
        
        if (empty($question) || $question['status'] != 'ok') {
            return 'Error: Question not found';
        }
        
        $category = $this->runCurlCall('/category/'.$question['record']['faqcategory_id'], 'GET');
        
        if (empty($category) || $category['status'] != 'ok') {
            return 'Error: Category not found';
        }
        
        $previousQuestion = $this->runCurlCall('/question/'.$id.'/prevquestion', 'GET');
        
        if (empty($previousQuestion) || $previousQuestion['status'] != 'ok') {
            $previousQuestion = FALSE;
        } else {
            $previousQuestion = $previousQuestion['record'];
        }
        
        $nextQuestion = $this->runCurlCall('/question/'.$id.'/nextquestion', 'GET');
        
        if (empty($nextQuestion) || $nextQuestion['status'] != 'ok') {
            $nextQuestion = FALSE;
        } else {
            $nextQuestion = $nextQuestion['record'];
        }
        
        return $this->template->loadTemplate('content/questionpage.tpl', array(
                    'category' => $category['record'],
                    'question' => $question['record'],
                    'previousQuestion' => $previousQuestion,
                    'nextQuestion' => $nextQuestion,
                ));
        
    }
    
    protected function search_get()
    {
        return $this->template->loadTemplate('content/search.tpl', array(
            'term' => $this->getValue('term')
        ));
    }
    
    protected function searchajax_get()
    {
        $this->setIsAjaxResponse();
        
        $result = $this->runCurlCall('/search', 'GET', array('term'=>$this->getValue('term')));
        
        if ($result['status'] == 'ok') {
            return json_encode($result['data']);
        }
    }
    
}
