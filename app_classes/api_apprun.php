<?php

use \SlimRunner\AppConfig as AppConfig;

class ApiAppRun extends \SlimRunner\SlimRunner
{
    protected function init()
    {
        $this->db = new AppDB(AppConfig::get('database', 'server'), AppConfig::get('database', 'name'), AppConfig::get('database', 'username'), AppConfig::get('database', 'password'));
        
        // Turn off templating - Will be using Json
        $this->setIsAjaxResponse();
        
        $this->registerRoutes(array(
            array('/',                          NULL,  'home',                      'get'),
            array('/category',                  NULL,  'Categories::list',          'get'),
            array('/category',                  NULL,  'Categories::new',           'post'),
            array('/category/namecheck',        NULL,  'Categories::namecheck',     'get'),
            array('/category/shorturlcheck',    NULL,  'Categories::shorturl',      'get'),
            array('/category/byshorturl/:url',  NULL,  'Categories::byShortUrl',    'get'),
            
            array('/category/:id',              NULL,  'Categories::action',        'get|put|delete',   array('id' => '\d+')),
            array('/category/:id/image',        NULL,  'Categories::image',         'post|delete',      array('id' => '\d+')),
            array('/category/:id/moveup',       NULL,  'Categories::moveup',        'patch',            array('id' => '\d+')),
            array('/category/:id/movedown',     NULL,  'Categories::movedown',      'patch',            array('id' => '\d+')),
            
            array('/category/:id/questions',    NULL,  'Questions::questions',      'get|post',         array('id' => '\d+')),
            array('/question/:id',              NULL,  'Questions::action',         'get|put|delete',   array('id' => '\d+')),
            array('/question/:id/moveup',       NULL,  'Questions::moveup',         'patch',            array('id' => '\d+')),
            array('/question/:id/movedown',     NULL,  'Questions::movedown',       'patch',            array('id' => '\d+')),
            array('/question/:id/prevquestion', NULL,  'Questions::prevquestion',   'get',              array('id' => '\d+')),
            array('/question/:id/nextquestion', NULL,  'Questions::nextquestion',   'get',              array('id' => '\d+')),
            
            array('/search',                    NULL,  'Questions::search',         'get'),
        ));
    }

    
    
    protected function home_get()
    {
        return json_encode(array('ping'=>'pong'));
    }
    
    
}
