<?php

use \SlimRunner\AppConfig as AppConfig;

class AppDB extends \Tohir\Database
{
    public function getTestData()
    {
        return $this->db->select('test');
    }
}
