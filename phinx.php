<?php

define('APPLICATION_PATH', realpath(dirname(__FILE__).'/'));
require_once APPLICATION_PATH.'/vendor/autoload.php';

use SlimRunner\AppConfig as AppConfig;
AppConfig::load(APPLICATION_PATH.'/config.ini');

return [
    'paths' => [
        'migrations' => APPLICATION_PATH.'/phinx_migrations'
    ],
    'environments' => [
        'default_migration_table' => 'phinx_migration_log',
        'default_database' => AppConfig::get('database', 'environment'),
        
        AppConfig::get('database', 'environment') => [
            'adapter'   => AppConfig::get('database', 'type'),
            'host'      => AppConfig::get('database', 'server'),
            'name'      => AppConfig::get('database', 'name'),
            'user'      => AppConfig::get('database', 'username'),
            'pass'      => AppConfig::get('database', 'password'),
            'port'      => AppConfig::get('database', 'port')
        ]
    ]
];