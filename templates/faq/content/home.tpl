<div class="row">
        
        {foreach $categories as $category}
        <div class="col-lg-4 text-center">
                <a href="/view/{$category.shorturl}">
                {if $category.hasimage =='Y'}
                        <img class="img-circle" src="{$apiUrl}/files/{$category.id}.png" width="140" height="140">
                {else}
                        <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="140" height="140">
                {/if}
                </a>
                <h2>{$category.name|escape}</h2>
                <p>{$category.description|escape}</p>
                <p><a class="btn btn-default" href="/view/{$category.shorturl}" role="button">View Category »</a></p>
        </div>
        {/foreach}
</div>