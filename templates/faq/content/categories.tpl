<h1>Manage Categories</h1>

{if !$categories}
    <p>No Categories have been setup</p>
{else}
<table class="table table-striped table-hover">
    <thead>
        <th class="text-center" width="200">Image</th>
        <th>Category Name</th>
        <th class="text-center" width="50">Change Image</th>
        <th class="text-center" width="50">Edit</th>
        <th class="text-center" width="50">Delete</th>
        <th class="text-center" width="50">Move Up</th>
        <th class="text-center" width="50">Move Down</th>
    </thead>
    
    <tbody>
        {foreach $categories as $category}
            <tr>
                <td>{if $category.hasimage=="Y"}<img src="{$apiUrl}/files/{$category.id}.png{if $lastid == $category.id}?{$random}{/if}" />{else}&nbsp;{/if}</td>
                <td>
                    <strong><a href="/view/{$category.shorturl}">{$category.name|escape}</a></strong>
                    <br />
                    <br />
                    {$category.description|escape|nl2br}
                </td>
                <td class="text-center"><a href="/categoryimage/{$category.id}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a></td>
                <td class="text-center"><a href="/editcategory/{$category.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="text-center"><a href="/deletecategory/{$category.id}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                <td class="text-center"><a href="/categorymoveup/{$category.id}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></td>
                <td class="text-center"><a href="/categorymovedown/{$category.id}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
{/if}