<h1>{$category.name|escape} <a href="/editcategory/{$category.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

<p>{$category.description|escape|nl2br}</p>


<h3>Questions <a href="/addquestion/{$category.id}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>

{if (!$questions)}
    <p>Category does not have any questions</p>
{else}
<table class="table table-striped table-hover">
    <thead>

        <th>Question</th>
        <th class="text-center" width="200">Options</th>
    </thead>
    
    <tbody>
    {foreach $questions as $question}
        <tr>
            <td><a href="/viewquestion/{$question.id}">{$question.question|escape}</a></td>
            <td class="text-center">
                <a href="/editquestion/{$question.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> &nbsp;
                <a href="/deletequestion/{$question.id}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a> &nbsp;
                <a href="/questionmoveup/{$question.id}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a> &nbsp;
                <a href="/questionmovedown/{$question.id}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a> &nbsp;
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
{/if}