<p>Category: <a href="/view/{$category.shorturl}">{$category.name|escape}</a></p>

<h1>{$question.question|escape} <a href="/editquestion/{$question.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></h1>

<p>{$question.answer}</p>

<table class="table">
    <tr>
        <td class="text-left" width="50%">
            {if $previousQuestion}
                &lt;&lt; <a href="/viewquestion/{$previousQuestion.id}">{$previousQuestion.question|escape}</a>
            {else}
                &nbsp;
            {/if}
        </td>
        
        <td class="text-right" width="50%">
            {if $nextQuestion}
                <a href="/viewquestion/{$nextQuestion.id}">{$nextQuestion.question|escape}</a> &gt;&gt;
            {else}
                &nbsp;
            {/if}
        </td>
    </tr>
</table>