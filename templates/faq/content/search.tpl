<h1>Search for: <span id="searchterm"></span></h1>

<input type="text" class="col-md-6" value="{$term|escape}" id="searchfield" />
 &nbsp;
<input type="button" value="Search" id="searchbutton" />

<h3>Search Results:</h3>

<div id="results">
    
</div>


<script type="text/javascript">
    
    jQuery( document ).ready(function() {
        jQuery('#searchbutton').click(function() {
            search(jQuery('#searchfield').val());
        });
        
        {if $term}
        jQuery('#searchbutton').click();
        {/if}
    });
    
    function search(term) {
        jQuery('#searchterm').text(term);
        
        jQuery.getJSON('/searchajax?term='+term, function( data ) {
            
            if (data.length == 0) {
                jQuery('#results').html('<p>No Results found</p>');
            } else {
                
                str = '<ol>';
                
                jQuery.each( data, function( i, item ) {
                    console.dir(item);
                    str += '<li><a href="/viewquestion/'+item.id+'"><strong>'+htmlEncode(item.question)+'</strong></a><br />Score: '+item.score+'</li>';
                });
                
                str += '<ol>';
                
                jQuery('#results').html(str);
            }
        });
    }
    
    function htmlEncode(value){
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return $('<div/>').text(value).html();
    }
    
</script>