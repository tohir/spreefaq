<!DOCTYPE html>

<html>
<head>
    
    <title>{$appTitle}</title>
    
    <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
    
    {* Bootstrap CSS *}
    <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bower/bootstrap/dist/css/bootstrap-theme.min.css">
    
    {* App CSS *}
    <link rel="stylesheet" href="/app/cerulean.css">
    <link rel="stylesheet" href="/app/app.css">
    
    {* jQuery *}
    <script type="text/javascript" src="/bower/jquery/dist/jquery.min.js"></script>
   
    {* jQuery Validation *}
    <script type="text/javascript" src="/bower/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/bower/jquery-validation/dist/additional-methods.min.js"></script>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="/bower/bootstrap/dist/js/bootstrap.min.js"></script>     
    
    <script type="text/javascript" src="/ckeditor/4.3.4/ckeditor.js"></script>
    <script type="text/javascript" src="/ckeditor/4.3.4/adapters/jquery.js"></script>
	
    {* App JS *}
    <script src="/app/common.js"></script>
    
    
    
</head>

<body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="/app/faq.png" />
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li {if $currentTab=="home"}class="active"{/if}>
							<a href="/">Home</a>
						</li>
						<li {if $currentTab=="categories"}class="active"{/if}>
							<a href="/categories">Manage Categories</a>
						</li>
						<li {if $currentTab=="addcategory"}class="active"{/if}>
							<a href="/addcategory">Add Category</a>
						</li>
						<li {if $currentTab=="addquestion"}class="active"{/if}>
							<a href="/addquestion">Add Question</a>
						</li>
						
					</ul>
					<form class="navbar-form navbar-right" role="search" method="get" action="/search">
						<div class="form-group">
							<input type="text" class="form-control" name="term" />
						</div> 
						<button type="submit" class="btn btn-default">
							Search
						</button>
					</form>
				</div>
				
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
            {$content}
		</div>
	</div>
</div>
    

</body>
</html>